\include "global.ly"

scoreTrompeta = \relative c'' {
  \global
  \key a \minor
  % Music follows here.

}

scoreTrompetaPart = \new Staff \with {
  instrumentName = "Trombe in C."
  midiInstrument = "trumpet"
} \scoreTrompeta

%\scoreTrompeta
