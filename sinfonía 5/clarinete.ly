\include "global.ly"

scoreClarinete = \relative c'' {
  \global
  \transposition bes
  \key f \major
  % Music follows here.

}

scoreClarinetePart = \new Staff \with {
  instrumentName = "Clarinetti in B."
  midiInstrument = "clarinet"
} \scoreClarinete

%\scoreClarinete
