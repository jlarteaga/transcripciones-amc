\include "global.ly"

scoreContrabajo = \relative c {
  \global
  \key c \minor
  % Music follows here.

}

scoreContrabajoPart = \new Staff \with {
  instrumentName = "Basso."
  midiInstrument = "contrabass"
} { \clef bass \scoreContrabajo }

%\scoreContrabajo
