\include "global.ly"

scoreViolinII = \relative c'' {
  \global
  \key c \minor
  % Music follows here.

}

scoreViolinIIPart = \new Staff \with {
  instrumentName = "Violino II."
  midiInstrument = "violin"
} \scoreViolinII

%\scoreViolinII
