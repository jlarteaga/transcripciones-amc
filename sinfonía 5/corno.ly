\include "global.ly"

scoreCorno = \relative c'' {
  \global
  \transposition es
  \key a \minor
  % Music follows here.

}

scoreCornoPart = \new Staff \with {
  instrumentName = "Corni in Es."
  midiInstrument = "french horn"
} \scoreCorno

%\scoreCorno
