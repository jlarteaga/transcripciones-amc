\version "2.19.82"

\header {
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "letter")
}


\include "global.ly"

\include "flauta.ly"
\include "oboe.ly"
\include "clarinete.ly"
\include "fagot.ly"
\include "corno.ly"
\include "trompeta.ly"
\include "timbales.ly"
\include "violinI.ly"
\include "violinII.ly"
\include "viola.ly"
\include "cello.ly"
\include "contrabajo.ly"

grandStaffViolin = \new GrandStaff <<
  \scoreViolinIPart
  \scoreViolinIIPart
>>

grandStaffBajos = \new GrandStaff <<
  \scoreCelloPart
  \scoreContrabajoPart
>>

staffGroupGeneral = \new StaffGroup <<
  \scoreFlautaPart
  \scoreOboePart
  \scoreClarinetePart
  \scoreFagotPart
  \scoreCornoPart
  \scoreTrompetaPart
  \scoreTimbalesPart
  \grandStaffViolin
  \scoreViolaPart
  \grandStaffBajos
>>

\score {
  \staffGroupGeneral
  \layout { }
  \midi { }
}
