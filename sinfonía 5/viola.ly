\include "global.ly"

scoreViola = \relative c' {
  \global
  \key c \minor
  % Music follows here.

}

scoreViolaPart = \new Staff \with {
  instrumentName = "Viola."
  midiInstrument = "viola"
} { \clef alto \scoreViola }

%\scoreViola
