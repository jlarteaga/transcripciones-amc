\include "global.ly"

scoreOboe = \relative c'' {
  \global
  \key c \minor
  % Music follows here.
}

scoreOboePart = \new Staff \with {
  instrumentName = "Oboi."
  midiInstrument = "oboe"
} \scoreOboe

%\scoreOboe
