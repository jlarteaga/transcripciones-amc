\include "global.ly"

scoreFlauta = \relative c'' {
  \global
  \key c \minor
  % Music follows here.

}

scoreFlautaPart = \new Staff \with {
  instrumentName = "Flauti."
  midiInstrument = "flute"
} \scoreFlauta

%\scoreFlauta
