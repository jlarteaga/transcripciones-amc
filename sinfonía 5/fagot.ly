\include "global.ly"

scoreFagot = \relative c {
  \global
  \key c \minor
  % Music follows here.

}

scoreFagotPart = \new Staff \with {
  instrumentName = "Fagotti."
  midiInstrument = "bassoon"
} { \clef bass \scoreFagot }

%\scoreFagot
