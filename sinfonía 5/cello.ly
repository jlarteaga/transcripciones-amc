\include "global.ly"

scoreCello = \relative c {
  \global
  \key c \minor
  % Music follows here.

}

scoreCelloPart = \new Staff \with {
  instrumentName = "Violoncello."
  midiInstrument = "cello"
} { \clef bass \scoreCello }

%\scoreCello
