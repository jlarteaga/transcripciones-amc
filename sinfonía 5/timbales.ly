\include "global.ly"

scoreTimbales = \relative c {
  \global
  \key a \minor
  % Music follows here.

}

scoreTimbalesPart = \new Staff \with {
  instrumentName = "Timpani in C.G."
  midiInstrument = "timpani"
} { \clef bass \scoreTimbales }

%\scoreTimbales
