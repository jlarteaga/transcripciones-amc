\include "global.ly"

scoreViolinI = \relative c'' {
  \global
  \key c \minor
  % Music follows here.

}

scoreViolinIPart = \new Staff \with {
  instrumentName = "Violino I."
  midiInstrument = "violin"
} \scoreViolinI

scoreViolinIPart
